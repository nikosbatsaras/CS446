\documentclass{article}

\usepackage{graphicx}               % To include figures (\includegraphics{...})
\usepackage{float}                  % To manually position figures
\usepackage[margin=1in]{geometry}   % To position title a bit higher
\usepackage{hyperref}               % To insert/customize urls
\usepackage{caption}		    % To use \captionof 

\graphicspath{{figures/}}

\hypersetup{
	colorlinks=true,
	linkcolor=black,
	filecolor=magenta,      
	urlcolor=blue,
}


\title{
	CS446 Project Report\\
	\LARGE Identifying bottlenecks on MaxineVM 
}
\author{
	Kalaentzis, Georgios\\
	\texttt{kalaentzis@csd.uoc.gr}
	\and
	Batsaras, Nikolaos\\
	\texttt{batsaras@csd.uoc.gr}
	}
\date{18th May 2018}


\begin{document}

\maketitle
\begin{figure}[h]
	\centering
	\includegraphics[scale=0.7]{javalogo.jpg}
\end{figure}

\newpage

\tableofcontents

\newpage

\section{Introduction}
\subsection{Virtual Machines examined}
In this project, we are examining the performance of two popular Java Virtual
Machines:
\begin{itemize}
	\item Oracle's Hotspot
	\item Maxine VM
\end{itemize}

\noindent
Hotspot is a production level Virtual Machine:

\begin{quote}
	\textit{The Java HotSpot Virtual Machine is a core component of the
	Java SE platform.  It implements the Java Virtual Machine
	Specification, and is delivered as a shared library in the Java Runtime
	Environment. As the Java bytecode execution engine, it provides Java
	runtime facilities, such as thread and object synchronization, on a
	variety of operating systems and architectures. It includes dynamic
	compilers that adaptively compile Java bytecodes into optimized machine
	instructions and efficiently manages the Java heap using garbage
	collectors, optimized for both low pause time and throughput. It
	provides data and information to profiling, monitoring and debugging
	tools and applications. \begin{flushright} Oracle's website
	\end{flushright}}
\end{quote}

\noindent
Maxine one the other hand, is a research project:

\begin{quote}
	\textit{The Maxine Virtual Machine is an open source virtual machine
	that is developed at the University of Manchester. It was previously
	developed by Oracle Labs (formerly Sun Microsystems Laboratories). The
	emphasis in Maxine's architecture is on modular design and code reuse
	in the name of flexibility, configurability, and productivity for
	industrial and academic virtual machine researchers. It is one of a
	growing number of Java Virtual Machines written entirely in Java in a
	meta-circular style (for example, Squawk and Jikes RVM).
	\begin{flushright} Wikipedia \end{flushright}}
\end{quote}

\subsection{Metrics}
\noindent
This project is an effort to understand the underlying architectural
differences of the two virtual machines and try to explain the performance
discrepancies they have. We performed an experimental study on both VMs using,
a variety of different metrics. These metrics can be categorized as follows:

\begin{center}
	\begin{tabular}{ |c|c|c| } 
		\hline
		\textbf{Time}    & \textbf{Garbage Collection}        & \textbf{Compilation}\\
		\hline
		Execution Time   & Number of garbage collector calls  & Number of compiled methods \\
		Compilation Time & Duration of garbage collection     & \\
		\hline
	\end{tabular}
\end{center}

\subsection{Benchmark Suite}
The benchmark suite we used is the \textit{Dacapo benchmark suite}:

\begin{quote}
	\textit{This benchmark suite is intended as a tool for Java
	benchmarking by the programming language, memory management and
	computer architecture communities. It consists of a set of open source,
	real world applications with non-trivial memory loads.
	\begin{flushright} Dacapo's website \end{flushright}}
\end{quote}

\noindent
The set of benchmarks was downloaded in the form of a single jar file and
simply imported into each VM like one. It contains a variety of different
benchmarks which is exactly what we were looking for. Some of the benchmarks
were a bit 'problematic' and were excluded from the upcoming analysis.

\subsection{Examination environment}
The environment we used to perform our experiments was a commodity desktop
computer with the following not so impressive specs:

\begin{center}
	\begin{tabular}{ |r|c|l|r| } 
		\hline
		\textbf{CPU}    & Intel Core i5-3470 CPU 3.20GHz & \textbf{L1d} & 32K   \\ 
		\textbf{RAM}    & 4GBs RAM                       & \textbf{L1i} & 32K   \\ 
		\textbf{OS}     & Ubuntu 16.04 (64-bit)          & \textbf{L2}  & 256K  \\ 
		\textbf{Kernel} & Linux 4.13.0-41-generic        & \textbf{L3}  & 6144K \\
		\hline
	\end{tabular}
\end{center}

\noindent
We understand that this is not the ideal machine to perform an experimental
analysis but we argue that for the kind of project we are working on,
consistency is more important. All experiments run on the same hardware, on,
pretty much, the same conditions. So the comparison results, if not entirely
fair, should at least be worth looking at.

\subsection{Study reproduction}
The material used and produced by this study is held in a private repository on
GitLab. Everything is available upon request. We wrote a number of different
scripts in order to produce our results, so everything is automatic and easy to
reproduce. In order to setup your environment for reproduction, you will need a
working installation of the following software:

\begin{itemize}
	\item Oracle's Hotspot VM \\(\url{http://www.oracle.com/technetwork/java/javase/tech/index-jsp-136373.html})
	\item Maxine VM (\url{https://github.com/beehive-lab/Maxine-VM})
	\item Perf (\url{https://perf.wiki.kernel.org/index.php/Main_Page})
	\item Gnuplot (\url{http://www.gnuplot.info/})
	\item Dacapo Benchmark suite (\url{http://dacapobench.org/})
\end{itemize}

\noindent
Obviously, a similar machine is also required to gather equivalent results. The
specs of our testing platform are shown in subsection 1.4.

\section{Performance Study}
\subsection{Preparation}
In order to prepare for our experimental analysis, we first spent some time
studying some online material, research papers, etc. After getting some basic
understanding about the research topic, we started discussing what are the
metrics of interest, that will guide us to meaningful results. We converged to a
fixed set of metrics and started to look for the appropriate tools that will
help us assemble the first draft of numbers.
\\
After installing the VMs, we took a look at the available flags of each VM in
order to extract the metrics of interest. The flags we used are:

\begin{itemize}
	\item Hotspot
		\begin{itemize}
			\item \textit{-XX:+PrintGC} To gather garbage collection information
			\item \textit{-XX:+CITime} To gather compilation information
			\item \textit{-Xms512M} To adjust the heap size
		\end{itemize}

		\pagebreak
	\item MaxineVM
		\begin{itemize}
			\item \textit{-XX:+TraceGCTime} To gather garbage collection information
			\item \textit{-XX:+PrintCompilationTime} To get compilation time
			\item \textit{-T1X:+PrintMetrics} To get the number of T1X compilations
			\item \textit{-C1X:+PrintMetrics} To get the number of C1X compilations
			\item \textit{-Xms512M} To adjust the heap size
		\end{itemize}
\end{itemize}

\noindent
The next step was to find a way to parse that output and keep only the needed
variables. For that purpose, we wrote some scripts. After running all the
scripts, the output is a \textit{Dataset} folder that holds the numbers for
each benchmark used. In order to visualize the output, we wrote some
\textit{Gnuplot} scripts to help us plot our results.

\noindent
Below we present our findings for both VMs.

\subsection{Observations}
In the process of studying the output of the benchmarks, we came across some
observations worth noting. In the initial run of the scripts, we decided to
test if there is any large significance between garbage collecting before each
iteration or not. The dacapo suite provides an option to turn off garbage
collection before executing a benchmark (enabled by default). The results
indicated that disabling the option did not have an important difference, thus
we decided to run all experiments by garbage collecting before each iteration,
giving a clearer execution time of each benchmark.  \\

\noindent
Another point to consider is the selection of heap size. By using an initial
heap size of 256MB on MaxineVM, garbage collection was a much slower process. A
complete run needed about 8 and a half hours and almost 7 hours were spent in
the garbage collector, with a total that exceeded 23 thousand GC calls. In
Hotspot, there was no worth noting performance difference between the two heap
sizes. Since it dominated the run time in MaxineVM, we concluded that we should
run on both virtual machines an initial heap size of 512MB.  \\

\noindent
We also needed to examine the result of T1X and C1X compilations outputted by
MaxineVM. In comparison to HotSpot, there is a large difference in the total of
methods compiled by each virtual machine. After research, we came to the
conclusion that there are two reasons that clarify the results. MaxineVM does not
feature an interpreter; instead it employs only Just-In-Time compilation, thus
explaining partially the increased total of compilations. Additionally, MaxineVM
is written entirely in Java, therefore it compiles parts of itself during the
runtime of the benchmarks, adding even more to the total compilations. \\

\noindent
Below are plots that demonstrate the time spent in garbage collection throughout each benchmark. \\

\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{mx_gc_time_256.png}
		\captionof{figure}{Maxine with 256M Heap size}
		\label{fig:mx256}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{mx_gc_time_512.png}
		\captionof{figure}{Maxine with 512M Heap size}
		\label{fig:mx512}
	\end{minipage}
\end{figure}


%\begin{center}
%	\begin{tabular}{ |r|c|c| }
%		\hline
%		\textbf{Benchmark}	& \textbf{T1X Methods}	& \textbf{C1X Methods}	\\
%		\hline
%		\textbf{avrora}		& 2259  		& 13837 		\\
%		\textbf{jython} 	& 6835 			& 14384 		\\
%		\textbf{lusearch}	& 1269			& 13548			\\
%		\textbf{sunflow}	& 2259			& 13547			\\
%		\textbf{fop}		& 5873			& 14376			\\
%		\textbf{luindex}	& 1778			& 13643			\\
%		\textbf{pm}		& 3740			& 14574			\\
%		\textbf{xalan}		& 4223			& 14341			\\
%		\hline
%	\end{tabular}
%\end{center}

\subsection{Results}


\noindent
Our main goal was to identify the bottlenecks on MaxineVM. We focus on two
major components that affect execution time, Just-In-Time compilation and
garbage collection. \\

\noindent
Initially, we wanted to experiment on how the warm-up phase impacts the clean
execution time. In order to simulate this, we ran each benchmark and increased
the number of warm-ups after each completion, up to 10 iterations. Intuitively,
we expected to see better execution times as the warm-up phase increases. Below,
we see the execution times of each virtual machine that verifies our
hypothesis. \\

\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{mx_exec_time.png}
		\captionof{figure}{Maxine execution time}
		\label{fig:mx_exec}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{hs_exec_time.png}
		\captionof{figure}{HotSpot execution time}
		\label{fig:hs_exec}
	\end{minipage}
\end{figure}


\noindent
Starting from JIT compilation, we expect that as the warm-up phase increases,
there are more JIT invocations in this phase that optimize the benchmark code.
At the final execution, the virtual machines have optimized the benchmark code
and there is no need to compile any other methods, resulting in clean execution
time of the benchmarks. \\

\noindent
As seen in the plots below, HotSpot spends more time in compilation, but has
better execution times than MaxineVM. Additionally, MaxineVM spends less time
in compilation but invokes the optimizer more times than Hotspot, concluding
that the latter provides a better quality of optimizations. Furthermore, apart
from the reasons stated in section 2.2, we believe that MaxineVM invokes the
C1X compiler many times, because it optimizes parts of itself during the
benchmark runs, throwing away optimized code if the code cache fills up. Thus,
when the benchmark runs on the next iteration, the virtual machine needs to
re-optimize the methods it flushed away.\\


\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{mx_comp_time.png}
		\captionof{figure}{Maxine compilation time}
		\label{fig:mx_comp}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{hs_comp_time.png}
		\captionof{figure}{HotSpot compilation time}
		\label{fig:hs_comp}
	\end{minipage}
\end{figure}

\pagebreak

\noindent
The other large component that affects execution time is garbage collection. In
order to minimize garbage collection during the execution phase, the dacapo
suite invokes the collector before running each benchmark. By measuring the
total duration of GC during the execution phase we could study how much garbage
collection affects the execution time. Below, we see the time spent in garbage
collection during the execution time. \\


\begin{figure}[H]
	\centering
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{mx_gc_time.png}
		\captionof{figure}{Maxine garbage collection time}
		\label{fig:mx_gc_time}
	\end{minipage}%
	\begin{minipage}{.5\textwidth}
		\centering
		\includegraphics[width=.8\linewidth]{hs_gc_time.png}
		\captionof{figure}{HotSpot garbage collection time}
		\label{fig:hs_gc_time}
	\end{minipage}
\end{figure}

\noindent
As seen in the plots, HotSpot's G1 garbage collector barely adds any time to
the execution time; all experiments indicate that it needs under one second to
complete its task in the execution phase. In contrast to MaxineVM's SemiSpace
garbage collector, in most benchmarks it needs a number of seconds, reaching up
to 20 seconds. This indicates that there is a large impact on the execution
time, creating a bottleneck on MaxineVM.

\section{Conclusion}

Concluding, both the just-in-time compiler and the garbage collector could
become bottlenecks for MaxineVM. The results indicate the SemiSpace garbage
collector critically affects the execution time. The image is less clearer in
just-in-time compilation, as we could only compare both virtual machines as
whole, instead of just the execution phase, due to the fact that we could not
isolate when compilation was happening. \\

\noindent
As a future note, we would like to research further why MaxineVM has such a
large number of C1X compilations, even though it has less compilation time than
HotSpot. Furthermore, since we didn't have the required time, we would like to
compare all of MaxineVM's available garbage collectors, in order to study
whether the other GC yield better results.


\end{document}
