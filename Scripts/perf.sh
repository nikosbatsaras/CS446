#!/usr/bin/env bash

dacapo=~/Desktop/dacapo-9.12-MR1-bach.jar
benchmarks=( avrora     \
             batik      \
             eclipse    \
             fop        \
             h2         \
             jython     \
             luindex    \
             lusearch   \
             pmd        \
             sunflow    \
             tomcat     \
             tradebeans \
             tradesoap  \
             xalan      \
             )
iterations=10
output=Dataset/PERF/

STARTTIME=$(date +%s)

echo
echo "-------- PERF BENCHMARKS START --------"

echo
echo "  - Max number of iterations:   $iterations"
echo "  - Total number of benchmarks: ${#benchmarks[@]}"

if [ ! -d "$output" ]; then mkdir "$output"; fi

for bench in ${benchmarks[@]}
do
	echo -n "# Iterations, "  >> "${output}/${bench}_perf.dat"
	echo -n "Cache-misses, "  >> "${output}/${bench}_perf.dat"
	echo -n "Branch-misses, " >> "${output}/${bench}_perf.dat"
	echo    "Page-faults"     >> "${output}/${bench}_perf.dat"
done

for iter in `seq 1 $iterations`
do
	echo
	echo "Iteration(s) = $iter"
	for bench in ${benchmarks[@]}
	do
		echo "    Benchmark: $bench"

		if [ "$bench" = "fop" ] || [ "$bench" = "luindex" ]
		then
			perf stat -e cache-misses,branch-misses,page-faults java -jar "$dacapo" "$bench" -n "$iter" > tmp.txt 2>&1
			cachemisses=`cat tmp.txt  | grep 'cache-misses'  | awk '{print $1}'`
			branchmisses=`cat tmp.txt | grep 'branch-misses' | awk '{print $1}'`
			pagefaults=`cat tmp.txt   | grep 'page-faults'   | awk '{print $1}'`
		else
			perf stat -e cache-misses,branch-misses,page-faults java -jar "$dacapo" "$bench" -n "$iter" --size=large > tmp.txt 2>&1
			cachemisses=`cat tmp.txt  | grep 'cache-misses'  | awk '{print $1}'`
			branchmisses=`cat tmp.txt | grep 'branch-misses' | awk '{print $1}'`
			pagefaults=`cat tmp.txt   | grep 'page-faults'   | awk '{print $1}'`
		fi

		echo -n "$iter $cachemisses" >> "${output}/${bench}_perf.dat"
		echo -n " $branchmisses" >> "${output}/${bench}_perf.dat"
		echo " $pagefaults" >> "${output}/${bench}_perf.dat"
	done
done

echo
echo "--------- PERF BENCHMARKS END ---------"
echo

ENDTIME=$(date +%s)

ELAPSEDTIME=$(($ENDTIME - $STARTTIME))

FORMATED="$(($ELAPSEDTIME / 3600))h:$(($ELAPSEDTIME % 3600 / 60))m:$(($ELAPSEDTIME % 60))s"

echo
echo "  Benchmark time elapsed: $FORMATED"
echo
