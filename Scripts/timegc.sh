#!/usr/bin/env bash

dacapo=~/Desktop/dacapo-9.12-MR1-bach.jar
benchmarks=( avrora     \
             batik      \
             eclipse    \
             fop        \
             h2         \
             jython     \
             luindex    \
             lusearch   \
             pmd        \
             sunflow    \
             tomcat     \
             tradebeans \
             tradesoap  \
             xalan      \
             )
iterations=10
times_output=Dataset/TIMES/
gc_output=Dataset/GC/

STARTTIME=$(date +%s)

echo
echo "-------- TIME & GC BENCHMARKS START --------"

echo
echo "  - Max number of iterations:   $iterations"
echo "  - Total number of benchmarks: ${#benchmarks[@]}"

if [ ! -d "$times_output" ]; then mkdir "$times_output"; fi
if [ ! -d "$gc_output" ]; then mkdir "$gc_output"; fi

for bench in ${benchmarks[@]}
do
	echo -n "# Iterations, "   >> "${times_output}/${bench}_times.dat"
	echo -n "Execution Time, " >> "${times_output}/${bench}_times.dat"
	echo    "Compilation Time" >> "${times_output}/${bench}_times.dat"

	echo -n "# Iterations, " >> "${gc_output}/${bench}_gcs.dat"
	echo    "Number of GCs"  >> "${gc_output}/${bench}_gcs.dat"
done

for iter in `seq 1 $iterations`
do
	echo
	echo "Iteration(s) = $iter"
	for bench in ${benchmarks[@]}
	do
		echo "    Benchmark: $bench"

		if [ "$bench" = "fop" ] || [ "$bench" = "luindex" ]
		then
			java -XX:+CITime -XX:+PrintGC -jar $dacapo $bench -n $iter > tmp.txt 2>&1
			exectime=`cat tmp.txt | grep 'PASSED' | awk '{print $7}'`
			exectime=`echo "scale=2; $exectime / 1000" | bc`
		else
			java -XX:+CITime -XX:+PrintGC -jar $dacapo $bench -n $iter --size=large > tmp.txt 2>&1
			exectime=`cat tmp.txt | grep 'PASSED' | awk '{print $7}'`
			exectime=`echo "scale=2; $exectime / 1000" | bc`
		fi
		
		echo -n "$iter $exectime" >> "${times_output}/${bench}_times.dat"

		jittime=`cat tmp.txt | grep 'Total compilation time' | sed -r 's/,/./g'`
		IFS=' ' read -a jittime <<< "$jittime"
		echo " ${jittime[4]}" >> "${times_output}/${bench}_times.dat"

		numofgcs=`cat tmp.txt \
			| sed -n "/===== DaCapo 9.12-MR1 $bench starting =====/,/===== DaCapo 9.12-MR1 $bench PASSED/p" \
			| grep "GC (Allocation Failure)" \
			| wc -l`
		echo "$iter $numofgcs" >> "${gc_output}/${bench}_gcs.dat"
	done
done

echo
echo "--------- TIME & GC BENCHMARKS END ---------"
echo

ENDTIME=$(date +%s)

ELAPSEDTIME=$(($ENDTIME - $STARTTIME))

FORMATED="$(($ELAPSEDTIME / 3600))h:$(($ELAPSEDTIME % 3600 / 60))m:$(($ELAPSEDTIME % 60))s"

echo
echo "  Benchmark time elapsed: $FORMATED"
echo
