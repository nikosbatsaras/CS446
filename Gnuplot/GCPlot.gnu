# Clear previous settings
unset  log
unset  label

# Set basic graph attributes
set    xtic       1
set    ytic       auto
set    title     "GCs per num of iterations"
set    xlabel    "Num of iterations"
set    ylabel    "Num of GCs"
set    terminal   pngcairo enhanced font "arial,11"
set    output    "Plots/gc_graph.png"

set style data histogram 
set style fill solid border -1

# Plot data in files *.dat
plot  "Dataset/GC/avrora_gcs.dat"      using 2:xticlabels(1) title "avrora",     \
      "Dataset/GC/batik_gcs.dat"       using 2:xticlabels(1) title "batik",      \
      "Dataset/GC/eclipse_gcs.dat"     using 2:xticlabels(1) title "eclipse",    \
      "Dataset/GC/fop_gcs.dat"         using 2:xticlabels(1) title "fop",        \
      "Dataset/GC/h2_gcs.dat"          using 2:xticlabels(1) title "h2",         \
      "Dataset/GC/jython_gcs.dat"      using 2:xticlabels(1) title "jython",     \
      "Dataset/GC/luindex_gcs.dat"     using 2:xticlabels(1) title "luindex",    \
      "Dataset/GC/lusearch_gcs.dat"    using 2:xticlabels(1) title "lusearch",   \
      "Dataset/GC/pmd_gcs.dat"         using 2:xticlabels(1) title "pmd",        \
      "Dataset/GC/sunflow_gcs.dat"     using 2:xticlabels(1) title "sunflow",    \
      "Dataset/GC/tomcat_gcs.dat"      using 2:xticlabels(1) title "tomcat",     \
      "Dataset/GC/tradebeans_gcs.dat"  using 2:xticlabels(1) title "tradebeans", \
      "Dataset/GC/tradesoap_gcs.dat"   using 2:xticlabels(1) title "tradesoap",  \
      "Dataset/GC/xalan_gcs.dat"       using 2:xticlabels(1) title "xalan"
